package com.julia.converter;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

class DownloadTask extends AsyncTask<String, Integer, DownloadTask.Result> {

    private DownloadCallback<String> downloadCallback;

    DownloadTask(DownloadCallback<String> callback) {
        setCallback(callback);

    }

    void setCallback(DownloadCallback<String> callback) {
        downloadCallback = callback;
    }

    static class Result {
        public String resultValue;
        public Exception exception;

        public Result(String resultValue) {
            this.resultValue = resultValue;
        }

        public Result(Exception exception) {
            this.exception = exception;
        }
    }


    @Override
    protected void onPreExecute() {
        if (downloadCallback == null)
            return;

        NetworkInfo networkInfo = downloadCallback.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnected() ||
                (networkInfo.getType() != ConnectivityManager.TYPE_WIFI
                        && networkInfo.getType() != ConnectivityManager.TYPE_MOBILE)) {


            downloadCallback.updateFromDownload(null);
            cancel(true);
        }
    }


    @Override
    protected DownloadTask.Result doInBackground(String... urls) {
        Result result = null;
        if (!isCancelled() && urls != null && urls.length > 0) {

            try {
                URL url = new URL(urls[0]);
                String resultString = downloadUrl(url);

                if (resultString != null) {
                    result = new Result(resultString);
                } else {
                    throw new IOException("No response received.");
                }
            } catch (Exception e) {
                result = new Result(e);
            }
        }
        return result;
    }


    private String downloadUrl(URL url) throws IOException {
        InputStream stream = null;
        HttpsURLConnection connection = null;
        String result = null;

        try {
            connection = (HttpsURLConnection) url.openConnection();

            connection.setReadTimeout(3000);
            connection.setConnectTimeout(3000);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);

            connection.connect();
            publishProgress(DownloadCallback.Progress.CONNECT_SUCCESS);
            int responseCode = connection.getResponseCode();

            if (responseCode != HttpsURLConnection.HTTP_OK) {
                throw new IOException("HTTP error code: " + responseCode);
            }

            stream = connection.getInputStream();
            publishProgress(DownloadCallback.Progress.GET_INPUT_STREAM_SUCCESS, 0);
            if (stream != null) {

                result = readStream(stream, 4096);
            }
        } finally {

            if (stream != null) {
                stream.close();
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }

    public String readStream(InputStream stream, int maxReadSize)
            throws IOException, UnsupportedEncodingException {

        Reader reader = new InputStreamReader(stream, "UTF-8");
        char[] rawBuffer = new char[maxReadSize];
        int readSize;

        StringBuffer buffer = new StringBuffer();

        while (((readSize = reader.read(rawBuffer)) != -1)) {

            buffer.append(rawBuffer, 0, readSize);
        }
        return buffer.toString();
    }

    @Override
    protected void onPostExecute(Result result) {
        if (result != null && downloadCallback != null) {
            if (result.exception != null) {
                downloadCallback.updateFromDownload(result.exception.getMessage());
            } else if (result.resultValue != null) {
                downloadCallback.updateFromDownload(result.resultValue);
            }
        }
    }

    @Override
    protected void onCancelled(Result result) {
    }
}