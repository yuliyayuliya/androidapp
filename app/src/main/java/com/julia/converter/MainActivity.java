package com.julia.converter;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;


public class MainActivity extends AppCompatActivity {

    final MainActivity self = this;   //! чтобы прокинуть в колбэк
    ArrayList<String> currenciesList; //! полученный список валют


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText inputValue = findViewById(R.id.editValue);
        final EditText outputValue = findViewById(R.id.editRes);
        final Spinner srcCurrency = findViewById(R.id.spinner);
        final Spinner resCurrency = findViewById(R.id.spinner2);

        // получаем список валют
        DownloadTask listTask = new DownloadTask(new DownloadCallback<String>() {
            @Override
            public void updateFromDownload(String result) {

                try {
                    JSONObject json = new JSONObject(result);
                    JSONObject currencies = json.getJSONObject("results");

                    Iterator<String> it = currencies.keys();

                    currenciesList = new ArrayList<>();

                    while (it.hasNext()) {

                        JSONObject cur = currencies.getJSONObject(it.next());

                        if (cur != null)
                            currenciesList.add(cur.getString("id"));
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(self,
                            android.R.layout.simple_spinner_item, currenciesList);

                    srcCurrency.setAdapter(adapter);
                    resCurrency.setAdapter(adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public NetworkInfo getActiveNetworkInfo() {
                ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                return cm.getActiveNetworkInfo();

            }
        });
        listTask.execute("https://free.currencyconverterapi.com/api/v6/currencies");

        resCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Ваш выбор: " + currenciesList.get(position), Toast.LENGTH_SHORT);

                if (srcCurrency.getSelectedItemPosition() == position)
                    srcCurrency.setSelection(position == 0 ? 1 : 0);

                toast.show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        srcCurrency.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast toast = Toast.makeText(getApplicationContext(),
                        "Ваш выбор: " + currenciesList.get(position), Toast.LENGTH_SHORT);

                if (resCurrency.getSelectedItemPosition() == position)
                    resCurrency.setSelection(position == 0 ? 1 : 0);

                toast.show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Button convertButton = findViewById(R.id.button);
        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                outputValue.setText("");

                if (inputValue.getText().toString().equals(""))
                    return;

                final String fromCurrency = srcCurrency.getSelectedItem().toString();
                final String toCurrency = resCurrency.getSelectedItem().toString();

                // запрашиваем текущий курс
                DownloadTask task = new DownloadTask(new DownloadCallback<String>() {
                    @Override
                    public void updateFromDownload(String result) {

                        if (inputValue.getText().toString().equals(""))
                            return;

                        double value = 1.0;

                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            value = jsonObject.getDouble(fromCurrency + "_" + toCurrency);
                        } catch (JSONException e) {
                        }

                        double res = value * Double.parseDouble(inputValue.getText().toString());
                        outputValue.setText(String.valueOf(res));
                    }

                    @Override
                    public NetworkInfo getActiveNetworkInfo() {
                        ConnectivityManager cm = (ConnectivityManager)
                                getSystemService(Context.CONNECTIVITY_SERVICE);
                        return cm.getActiveNetworkInfo();

                    }

                });

                task.execute("https://free.currencyconverterapi.com/api/v6/convert?q=" +
                        fromCurrency + "_" + toCurrency + "&compact=ultra");
            }
        });

    }

}

